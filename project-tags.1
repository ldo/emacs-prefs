.TH "PROJECT-TAGS" "1" "2023-11-05" "Geek Central" "Emacs Tools"
.
.SH NAME
project\-tags \(em easy navigation to definitions of names in a source-code project
.SH SYNOPSYS
\fBproject\-tags \fI[cmd] [args ...]\fR
.SH DESCRIPTION
.PP
.B project\-tags
is a command-line tool for managing a cache database of name definitions
as output by the
.BR etags (1)
command. It provides subcommands for generating and updating the
cache, and also for looking up names, to allow quick navigation to the
points in the relevant source files where those names are defined. The
cache is maintained in an
.UR https://sqlite.org/
SQLite
.UE
database.
.
In the following, a
.I project
can be identified by a directory containing a
.B .git
subdirectory, as created by the
.UR https://git-scm.com/
Git
.UE
version-control system. You can specify such a project directory directly,
or you can specify a subdirectory or a file somewhere within that
directory;
.B project\-tags
will automatically search upwards through parent directories as necessary
until it finds one containing the
.B .git
subdirectory. Additionally, you can use the \fBadd-project\fR command
with the \fB\-\-asis\fR option to add a directory directly. This allows
you to do things like add \fB/usr/include\fR as a project,
for doing convenient searches for the definitions of system symbols.
.
.SH SUBCOMMANDS
.
The available commands are:
.
.TP
\fBhelp \fR[\fIcmd\fR]
with \fIcmd\fR, displays help for the specified command. If \fIcmd\fR is omitted,
shows a list of valid commands.
.
.TP
\fBadd\-project \fR[\fB\-\-asis\fR]\fB \fIproject\fR
explicitly adds the \fIproject\fR directory. The \fB\-\-asis\fR option
skips the search for a “\fB.git\fR” subdirectory, and adds the
specified directory directly as a project. Note that the \fBrebuild\fR
command will automatically add a project if not already present in the
cache, but it always needs to find a “\fB.git\fR” subdirectory.
.
.TP
\fBrebuild\ \fIproject\fR
Builds or rebuilds the cache for the specified project. This runs the
.BR etags (1)
command over the files in the project, and processes its output to generate
the database entries. The files’ last-modified timestamps are checked against
those recorded in the database, and only new/changed files are processed,
while cache entries matching files which no longer exist are deleted. So
running this command over a project where nothing has changed should be quite
quick.
.
But note, however, that when re-checking-out files, Git will give them new
modification timestamps, even if their contents have not changed, and this
will trigger a reprocessing of these files.
.
.TP
\fBlookup \fI[\fB\-\-json\fI] \fIproject symbol\fR
Looks up the definition of \fIsymbol\fR in the cache entries for the
specified project. If \fB\-\-json\fR is specified, then the output is
in JSON format. This can be easily parsed with the \fBjson\-parse\-string\fR
command in Emacs Lisp, as for example in the accompanying \fBfind\-def\fR
function.
.
.TP
.B list
Shows the names of all projects with entries in the cache.
.
.TP
.B delete \fIproject\fR
Deletes information for the specified project from the cache.
.
.TP
.B delete\-missing
Deletes information for all projects from the cache whose corresponding
base directories cannot be found.
.
.TP
.B gc
Compacts the cache database using the SQLite \fBvacuum\fR command, and reports
the before/after database sizes and how much space was saved. Compaction
also happens automatically every time one of the project-deletion commands
is used.
.
.SH EXAMPLES
.
.RS
\fBproject\-tags rebuild ~/projects/my\-project\fR
.RE
.
Builds or rebuilds the tags cache for the specified project directory.
.
.RS
\fBproject\-tags lookup \-\-json ~/projects/my\-project my_func\fR
.RE
.
looks up the definition of the symbol \fBmy_func\fR in the given
project and outputs it in JSON format.
.
.SH FILES
The tags database is kept in \fB$XDG_CACHE_HOME/project\-tags/tags.db\fR, or
\fB~/.cache/project\-tags/tags.db\fR if \fB$XDG_CACHE_HOME\fR is not defined.
.
.SH COPYRIGHT
.
Copyright 2023 Lawrence D'Oliveiro <ldo@geek\-central.gen.nz>.
Licensed under CC-BY <http://creativecommons.org/licenses/by/4.0/>.
