;+
; Do quoting/unquoting of multiline string literals, as for example
; when I want to embed lines of one language (JavaScript) into a
; program in another language (Python).
;
; Copyright 2022 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
; Licensed under CC-BY <http://creativecommons.org/licenses/by/4.0/>.
;-

(defun multiline-quote (beg end)
    "does quoting of the selected region of text."
    (interactive "*r")
    (unless (use-region-p)
        (ding)
        (keyboard-quit)
    ) ; unless
    (let ((end_marker (copy-marker end)) prefix-indent thisindent)
        (save-excursion
            (goto-char beg)
            (beginning-of-line)
            (while (< (point) (marker-position end_marker))
                ; find minimum indentation level, excluding empty lines.
                ; Greater indentation than this will be included in
                ; quoted string literal lines, to preserve indentation
                ; of quoted code.
                (looking-at " *")
                (setq thisindent (length (match-string 0)))
                (unless (= thisindent 0)
                    (setq prefix-indent
                        (cond
                            ((eq prefix-indent nil)
                                thisindent
                            )
                            (t
                                (min prefix-indent thisindent)
                            )
                        ) ; cond
                    )
                ) ; unless
                (forward-line)
            ) ; while
            (when (eq prefix-indent nil)
                (ding)
                (keyboard-quit)
            ) ; when
            (goto-char beg)
            (beginning-of-line)
            (while (< (point) (marker-position end_marker))
                (cond
                    ((looking-at " ")
                        (forward-char prefix-indent)
                        (insert "\"")
                        (while (looking-at "[^\"\n]*?\\([\"\\\\]\\)")
                            ; escape any backslashes or quotes within line
                            (goto-char (match-beginning 1))
                            (insert "\\")
                            (forward-char)
                        ) ; while
                        (end-of-line)
                        (insert "\\n\"")
                    )
                    (t
                        ; replace empty lines with “"\n"”, suitably indented
                        (dotimes (i prefix-indent)
                            (insert " ")
                        ) ; dotimes
                        (insert "\"\\n\"")
                    )
                ) ; cond
                (forward-line)
            ) ; while
        ) ; save-excursion
    ) ; let
) ; multiline-quote

(defun multiline-unquote (beg end)
    "undoes quoting of the selected region of text."
    (interactive "*r")
    (unless (use-region-p)
        (ding)
        (keyboard-quit)
    ) ; unless
    (let
        ((end_marker (copy-marker end)))
        (save-excursion
            (goto-char beg)
            (beginning-of-line)
            (while (< (point) (marker-position end_marker))
                (unless (looking-at "\\( *\\)\".*\\\\n\"$")
                    ; give up on hitting a line with a format I don’t expect
                    (ding)
                    (keyboard-quit)
                ) ; unless
                (goto-char (match-end 1))
                (delete-char 1) ; delete “"” at start of quoted line
                (save-excursion
                    (end-of-line)
                    (delete-backward-char 3) ; delete “\n"” at end of line
                ) ; save-excursion
                (while (looking-at "\\([^\\\\\n]*?\\)\\\\")
                    ; strip backslash-escaping within line
                    (goto-char (match-end 1))
                    (delete-char 1)
                    (forward-char 1)
                      ; skip character that was being backslash-escaped (might also be backslash)
                ) ; while
                (beginning-of-line)
                ; if line is now empty except for blanks, get rid of the blanks
                (when (looking-at "\\( +\\)$")
                    (delete-char (length (match-string 1)))
                ) ; when
                (forward-line)
            ) ; while
        ) ; save-excursion
    ) ; let
) ; multiline-unquote
