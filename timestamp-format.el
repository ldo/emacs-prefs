;+
; Conversion of timestamps between [hh:]mm:ss format and integer seconds.
;
; Note “mstr”, “loop” and “get-run” macros are defined in main dotemacs file.
;
; Copyright 2023 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
; Licensed under CC-BY <http://creativecommons.org/licenses/by/4.0/>.
;-

(defun timestamp-from-hhmmss ()
    "looks for a string in [hh:]mm:ss format at point or in region, and replaces with seconds."
    (interactive "*")
    (let (beg end str pieces seconds)
        (cond
            ((use-region-p)
                (setq beg (region-beginning))
                (setq end (region-end))
                (setq str (buffer-substring beg end))
                (unless
                    (string-match "^[0-9\\:]+$" str)
                    (setq str nil)
                ) ; unless
            )
            (t
                (get-run (lambda () (looking-at "[0-9\\:]")) beg end)
                (cond
                    ((eq beg end)
                        (setq str nil)
                    )
                    (t
                        (setq str (buffer-substring beg end))
                    )
                ) ; cond
            )
        ) ; cond
        (cond
            (str
                (setq pieces (split-string str ":"))
                (setq seconds 0)
                (dolist (val pieces)
                    (setq seconds (+ (* seconds 60) (string-to-number val)))
                ) ; dolist
                (delete-region beg end)
                (insert (number-to-string seconds))
            )
            (t
                (ding)
            )
        ) ; cond
    ) ; let
) ; timestamp-from-hhmmss

(defun timestamp-to-hhmmss ()
    (mstr
        "looks for an integer representing seconds in region or at point,"
        " and converts to [hh:]mm:ss format."
    )
    (interactive "*")
    (let (beg end seconds hours minutes)
        (cond
            ((use-region-p)
                (setq beg (region-beginning))
                (setq end (region-end))
                (setq seconds (buffer-substring beg end))
                (unless
                    (string-match "^[0-9]+$" seconds)
                    (setq seconds nil)
                ) ; unless
            )
            (t
                (get-run (lambda () (looking-at "[0-9]")) beg end)
                (cond
                    ((eq beg end)
                        (setq seconds nil)
                    )
                    (t
                        (setq seconds (buffer-substring beg end))
                    )
                ) ; cond
            )
        ) ; cond
        (cond
            (seconds
                (setq seconds (string-to-number seconds))
                (setq hours (/ seconds 3600))
                (setq seconds (mod seconds 3600))
                (delete-region beg end)
                (when (/= hours 0)
                    (insert (number-to-string hours))
                    (insert ":")
                ) ; when
                (setq minutes (/ seconds 60))
                (setq seconds (mod seconds 60))
                (insert
                    (format
                        (format "%%%sd:%%02d" (cond ((/= hours 0) "02") (t "" )))
                        minutes seconds
                    )
                )
            )
            (t
                (ding)
            )
        ) ; cond
    ) ; let
) ; timestamp-to-hhmmss
