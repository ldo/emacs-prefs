;;; -*- lexical-binding: t; -*-
;+
; Convert regular text to/from alternative forms of alphanumeric characters
; (parenthesized, circled, mathematical etc). Not all of each type might
; be available, and this may also depend on the font.
;
; This module defines a bunch of functions with names like “to-xxx”
; to convert a selected text region to the specified category of
; alternative alphanumerics. There is also a single function
; “from-alt-alphanumerics” to convert all forms of alternative
; alphanumerics to regular forms.
;
; Relevant Unicode ranges:
;    U+2102             -- double-struck capital c
;    U+210D             -- double-struck capital h
;    U+2115             -- double-struck capital n
;    U+2119             -- double-struck capital p
;    U+211A             -- double-struck capital q
;    U+211D             -- double-struck capital r
;    U+2124             -- double-struck capital z
;    U+2145             -- double-struck italic capital d
;    U+2146             -- double-struck italic small d
;    U+2147             -- double-struck italic small e
;    U+2148             -- double-struck italic small i
;    U+2149             -- double-struck italic small j
;    U+2460 .. U+2468   -- circled digit [1 .. 9] (no zero)
;    U+2474 .. U+247C   -- parenthesized digit [1 .. 9] (no zero)
;    U+2488 .. U+2490   -- digit [1 .. 9] full stop
;    U+249C .. U+24B5   -- parenthesized latin small letter [a .. z]
;    U+24B6 .. U+24CF   -- circled latin capital letter [a .. z]
;    U+24D0 .. U+24E9   -- circled latin small letter [a .. z]
;    U+24F5 .. U+24FD   -- double circled digit [1 .. 9] (no zero)
;    U+24FF             -- negative circled digit zero
;    U+2776 .. U+277E   -- dingbat negative circled digit [1 .. 9] (no zero)
;    U+2780 .. U+2788   -- dingbat circled sans-serif digit [1 .. 9]
;    U+278A .. U+2792   -- dingbat negative circled sans-serif digit [1 .. 9]
;    U+FF21 .. U+FF3A   -- fullwidth latin capital letter [a .. z]
;    U+FF41 .. U+FF5A   -- fullwidth latin small letter [a .. z]
;    U+1D400 .. U+1D419 -- mathematical bold capital letter [a .. z]
;    U+1D41A .. U+1D433 -- mathematical bold small letter [a .. z]
;    U+1D434 .. U+1D44D -- mathematical italic capital letter [a .. z]
;    U+1D44E .. U+1D467 -- mathematical italic small letter [a .. z]
;    U+1D468 .. U+1D481 -- mathematical bold italic capital letter [a .. z]
;    U+1D482 .. U+1D49B -- mathematical bold italic small letter [a .. z]
;    U+1D49C .. U+1D4B5 -- mathematical script capital letter [a .. z] -- gaps filled from SCRIPT CAPITAL LETTER xxx
;    U+1D4B6 .. U+1D4CF -- mathematical script small letter [a .. z] -- gaps filled from SCRIPT SMALL LETTER xxx
;    U+1D4D0 .. U+1D4E9 -- mathematical bold script capital letter [a .. z]
;    U+1D4EA .. U+1D503 -- mathematical bold script capital letter [a .. z]
;    U+1D504 .. U+1D51D -- mathematical fraktur capital letter [a .. z] -- gaps filled from BLACK-LETTER CAPITAL xxx
;    U+1D51E .. U+1D537 -- mathematical fraktur small letter [a .. z]
;    U+1D538 .. U+1D551 -- mathematical double-struck capital letter [a .. z],
;                          omitting ones that have individual double-struck codes
;    U+1D552 .. U+1D56B -- mathematical double-struck small letter [a .. z]
;    U+1D56C .. U+1D585 -- mathematical bold fraktur capital letter [a .. z] (no digits)
;    U+1D7D8 .. U+1D7E1 -- mathematical double-struck digit [0 .. 9]
;    U+1D586 .. U+1D59F -- mathematical bold fraktur small letter [a .. z] (no digits)
;    U+1D5A0 .. U+1D5B9 -- mathematical sans-serif capital letter [a .. z]
;    U+1D5BA .. U+1D5D3 -- mathematical sans-serif small letter [a .. z]
;    U+1D5D4 .. U+1D5ED -- mathematical sans-serif bold capital letter [a .. z]
;    U+1D5EE .. U+1D607 -- mathematical sans-serif bold small letter [a .. z]
;    U+1D608 .. U+1D621 -- mathematical sans-serif italic capital letter [a .. z] (no digits)
;    U+1D622 .. U+1D63B -- mathematical sans-serif italic small letter [a .. z] (no digits)
;    U+1D63C .. U+1D655 -- mathematical sans-serif bold italic capital letter [a .. z] (no digits)
;    U+1D656 .. U+1D66F -- mathematical sans-serif bold italic small letter [a .. z] (no digits)
;    U+1D670 .. U+1D689    mathematical monospace capital letter [a .. z]
;    U+1D68A .. U+1D6A3    mathematical monospace small letter [a .. z]
;    U+1D7CE .. U+1D7D7    mathematical bold digit [0 .. 9]
;    U+1D7D8 .. U+1D7E1    mathematical double-struck digit [0 .. 9]
;    U+1D7E2 .. U+1D7EB -- mathematical sans-serif digit [0 .. 9]
;    U+1D7EC .. U+1D7F5    mathematical sans-serif bold digit [0 .. 9]
;    U+1D7F6 .. U+1D7FF    mathematical monospace digit [0 .. 9]
;    U+1F100            -- digit 0 full stop
;    U+1F101 .. U+1F10A -- digit [0 .. 9] comma
;    U+1F10B            -- dingbat circled sans-serif digit zero
;    U+1F10C            -- dingbat negative circled sans-serif digit zero
;    U+1F110 .. U+1F129 -- parenthesized latin capital letter [a .. z]
;    U+1F130 .. U+1F149 -- squared latin capital letter [a .. z]
;    U+1F150 .. U+1F169 -- negative circled latin capital letter [a .. z]
;    U+1F170 .. U+1F189 -- negative squared latin capital letter [a .. z]
;
; Copyright 2023 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
; Licensed under CC-BY <http://creativecommons.org/licenses/by/4.0/>.
;-

(defvar alt-alphanumerics (make-hash-table :test 'equal))
  ; table of forward-mapping tables
(defvar unalt-alphanumerics (make-hash-table)) ; common reverse-mapping table
(dolist
    ; initialize forward and reverse mapping tables
    (rangedef
        '( ; name reg_lo alt_lo [alt_hi]
            ("double-struck" ?C ?\N{U+2102})
            ("double-struck" ?H ?\N{U+210D})
            ("double-struck" ?N ?\N{U+2115})
            ("double-struck" ?P ?\N{U+2119})
            ("double-struck" ?Q ?\N{U+211A})
            ("double-struck" ?R ?\N{U+211D})
            ("double-struck" ?Z ?\N{U+2124})
            ("double-struck" ?0 ?\N{U+1D7D8} ?\N{U+1D7E1})

            ("double-struck italic" ?D ?\N{U+2145})
            ("double-struck italic" ?d ?\N{U+2146})
            ("double-struck italic" ?e ?\N{U+2147})
            ("double-struck italic" ?i ?\N{U+2148})
            ("double-struck italic" ?j ?\N{U+2149})

            ("circled" ?1 ?\N{U+2460} ?\N{U+2468}) ; no zero
            ("circled" ?A ?\N{U+24B6} ?\N{U+24CF})
            ("circled" ?a ?\N{U+24D0} ?\N{U+24E9})
            ("double circled" ?1 ?\N{U+24F5} ?\N{U+24FD}) ; digits only, no zero
            ("negative circled" ?0 ?\N{U+24FF}) ; no lowercase
            ("dingbat negative circled" ?1 ?\N{U+2776} ?\N{U+277E}) ; digits only, no zero
            ("dingbat circled sans-serif" ?1 ?\N{U+2780} ?\N{U+2788}) ; digits only
            ("dingbat negative circled sans-serif" ?1 ?\N{U+278A} ?\N{U+2792}) ; digits only
            ("dingbat circled sans-serif" ?0 ?\N{U+1F10B}) ; digits only
            ("dingbat negative circled sans-serif" ?0 ?\N{U+1F10C}) ; digits only
            ("squared" ?A ?\N{U+1F130} ?\N{U+1F149}) ; no lowercase, no digits
            ("negative circled" ?A ?\N{U+1F150} ?\N{U+1F169}) ; no lowercase
            ("negative squared" ?A ?\N{U+1F170} ?\N{U+1F189}) ; no lowercase, no digits

            ("parenthesized" ?1 ?\N{U+2474} ?\N{U+247C}) ; no zero
            ("parenthesized" ?a ?\N{U+249C} ?\N{U+24B5})
            ("parenthesized" ?A ?\N{U+1F110} ?\N{U+1F129})

            ("full stop" ?1 ?\N{U+2488} ?\N{U+2490}) ; digits only
            ("full stop" ?0 ?\N{U+1F100} ?\N{U+1F100})
            ("comma" ?0 ?\N{U+1F101} ?\N{U+1F10A}) ; digits only

            ("fullwidth" ?A ?\N{U+FF21} ?\N{U+FF3A}) ; no digits
            ("fullwidth" ?a ?\N{U+FF41} ?\N{U+FF5A}) ; no digits

            ("mathematical bold" ?A ?\N{U+1D400} ?\N{U+1D419})
            ("mathematical bold" ?a ?\N{U+1D41A} ?\N{U+1D433})

            ("mathematical italic" ?A ?\N{U+1D434} ?\N{U+1D44D}) ; no digits
            ("mathematical italic" ?a ?\N{U+1D44E} ?\N{U+1D467}) ; no digits

            ("mathematical bold italic" ?A ?\N{U+1D468} ?\N{U+1D481}) ; no digits
            ("mathematical bold italic" ?a ?\N{U+1D482} ?\N{U+1D49B}) ; no digits

            ("mathematical script" ?A ?\N{U+1D49C}) ; no digits, note gaps
            ("mathematical script" ?B ?\N{U+212C}) ; SCRIPT CAPITAL B
            ("mathematical script" ?C ?\N{U+1D49E} ?\N{U+1D49F})
            ("mathematical script" ?E ?\N{U+2130}) ; SCRIPT CAPITAL E
            ("mathematical script" ?F ?\N{U+2131}) ; SCRIPT CAPITAL F
            ("mathematical script" ?G ?\N{U+1D4A2})
            ("mathematical script" ?H ?\N{U+210B}) ; SCRIPT CAPITAL H
            ("mathematical script" ?I ?\N{U+2110}) ; SCRIPT CAPITAL I
            ("mathematical script" ?J ?\N{U+1D4A5} ?\N{U+1D4A6})
            ("mathematical script" ?L ?\N{U+2112}) ; SCRIPT CAPITAL L
            ("mathematical script" ?M ?\N{U+2133}) ; SCRIPT CAPITAL M
            ("mathematical script" ?N ?\N{U+1D4A9} ?\N{U+1D4AC})
            ; ("mathematical script" ?P ?\N{U+2118}) ; SCRIPT CAPITAL P
            ("mathematical script" ?R ?\N{U+211B}) ; SCRIPT CAPITAL R
            ("mathematical script" ?S ?\N{U+1D4AE} ?\N{U+1D4B5})
            ("mathematical script" ?a ?\N{U+1D4B6} ?\N{U+1D4B9})
            ("mathematical script" ?e ?\N{U+212F}) ; SCRIPT SMALL E
            ("mathematical script" ?f ?\N{U+1D4BB})
            ("mathematical script" ?g ?\N{U+212A}) ; SCRIPT SMALL G
            ("mathematical script" ?h ?\N{U+1D4BD} ?\N{U+1D4C3})
            ; ("mathematical script" ?l ?\N{U+2113}) ; SCRIPT SMALL L
            ("mathematical script" ?o ?\N{U+2134}) ; SCRIPT SMALL O
            ("mathematical script" ?p ?\N{U+1D4C5} ?\N{U+1D4CF})

            ("mathematical bold script" ?A ?\N{U+1D4D0} ?\N{U+1D4E9}) ; no digits
            ("mathematical bold script" ?a ?\N{U+1D4EA} ?\N{U+1D503}) ; no digits

            ("mathematical fraktur" ?A ?\N{U+1D504} ?\N{U+1D505}) ; no digits, note gaps
            ("mathematical fraktur" ?C ?\N{U+212D}) ; BLACK-LETTER CAPITAL C
            ("mathematical fraktur" ?D ?\N{U+1D507} ?\N{U+1D50A})
            ("mathematical fraktur" ?H ?\N{U+210C}) ; BLACK-LETTER CAPITAL H
            ("mathematical fraktur" ?I ?\N{U+2111}) ; BLACK-LETTER CAPITAL I
            ("mathematical fraktur" ?J ?\N{U+1D50D} ?\N{U+1D514})
            ("mathematical fraktur" ?R ?\N{U+211C}) ; BLACK-LETTER CAPITAL R
            ("mathematical fraktur" ?S ?\N{U+1D516} ?\N{U+1D51C})
            ("mathematical fraktur" ?Z ?\N{U+2128}) ; BLACK-LETTER CAPITAL Z
            ("mathematical fraktur" ?a ?\N{U+1D51E} ?\N{U+1D537}) ; no digits

            ("double-struck" ?A ?\N{U+1D538} ?\N{U+1D539})
            ("double-struck" ?D ?\N{U+1D53B} ?\N{U+1D53E})
            ("double-struck" ?I ?\N{U+1D540} ?\N{U+1D544})
            ("double-struck" ?O ?\N{U+1D546})
            ("double-struck" ?S ?\N{U+1D54A} ?\N{U+1D550})
            ("double-struck" ?a ?\N{U+1D552} ?\N{U+1D56B}) ; including mathematical double-struck

            ("mathematical bold fraktur" ?A ?\N{U+1D56C} ?\N{U+1D585}) ; no digits
            ("mathematical bold fraktur" ?a ?\N{U+1D586} ?\N{U+1D59F}) ; no digits

            ("mathematical sans-serif" ?A ?\N{U+1D5A0} ?\N{U+1D5B9})
            ("mathematical sans-serif" ?a ?\N{U+1D5BA} ?\N{U+1D5D3})
            ("mathematical sans-serif bold" ?A ?\N{U+1D5D4} ?\N{U+1D5ED})
            ("mathematical sans-serif bold" ?a ?\N{U+1D5EE} ?\N{U+1D607})
            ("mathematical sans-serif italic" ?A ?\N{U+1D608} ?\N{U+1D621}) ; no digits
            ("mathematical sans-serif italic" ?a ?\N{U+1D622} ?\N{U+1D63B}) ; no digits
            ("mathematical sans-serif bold italic" ?A ?\N{U+1D63C} ?\N{U+1D655}) ; no digits
            ("mathematical sans-serif bold italic" ?a ?\N{U+1D656} ?\N{U+1D66F}) ; no digits

            ("mathematical monospace" ?A ?\N{U+1D670} ?\N{U+1D689})
            ("mathematical monospace" ?a ?\N{U+1D68A} ?\N{U+1D6A3})

            ("mathematical bold" ?0 ?\N{U+1D7CE} ?\N{U+1D7D7})
            ("mathematical double-struck" ?0 ?\N{U+1D7D8} ?\N{U+1D7E1})
            ("mathematical sans-serif" ?0 ?\N{U+1D7E2} ?\N{U+1D7EB})
            ("mathematical sans-serif bold" ?0 ?\N{U+1D7EC} ?\N{U+1D7F5})
            ("mathematical monospace" ?0 ?\N{U+1D7F6} ?\N{U+1D7FF})

            ; ref <https://en.wikipedia.org/wiki/Rotated_letter>
            ("turned" ?A ?\N{U+2C6F})
            ("turned" ?B ?\N{U+A4ED}) ; LISU LETTER GHA
            ("turned" ?C ?\N{U+0186})
            ("turned" ?D ?\N{U+A4F7}) ; LISU LETTER OE
            ("turned" ?E ?\N{U+018E})
            ("turned" ?F ?\N{U+2132})
            ("turned" ?G ?\N{U+2141})
            ("turned" ?H ?H)
            ("turned" ?I ?I)
            ("turned" ?J ?\N{U+A4E9}) ; LISU LETTER FA
            ("turned" ?K ?\N{U+A7B0})
            ("turned" ?L ?\N{U+A780}) ; could also use U+2142
            ("turned" ?M ?\N{U+019C})
            ("turned" ?N ?N)
            ("turned" ?O ?O)
            ("turned" ?P ?\N{U+0500}) ; CYRILLIC CAPITAL LETTER KOMI DE
            ; ?Q -- none
            ("turned" ?R ?\N{U+A4E4}) ; LISU LETTER ZA
            ("turned" ?S ?S)
            ("turned" ?T ?\N{U+A7B1})
            ("turned" ?U ?\N{U+0548}) ; ARMENIAN CAPITAL LETTER VO
            ("turned" ?V ?\N{U+0245})
            ("turned" ?W ?M) ; approx
            ("turned" ?X ?X)
            ("turned" ?Y ?\N{U+2144})
            ("turned" ?Z ?Z)

            ("turned" ?a ?\N{U+0250})
            ("turned" ?b ?q) ; approx
            ("turned" ?c ?\N{U+0254})
            ("turned" ?d ?p) ; approx
            ("turned" ?e ?\N{U+01DD})
            ("turned" ?f ?\N{U+214E})
            ("turned" ?g ?\N{U+1D77})
            ("turned" ?h ?\N{U+0265}) ; ?\N{U+A78D}
            ("turned" ?i ?\N{U+1D09})
            ("turned" ?j ?\N{U+027E}) ; LATIN SMALL LETTER R WITH FISHHOOK
            ("turned" ?k ?\N{U+029E})
            ("turned" ?l ?\N{U+A781})
            ("turned" ?m ?\N{U+026F})
            ("turned" ?n ?u) ; approx
            ("turned" ?o ?o)
            ("turned" ?p ?d) ; approx
            ("turned" ?q ?b) ; approx
            ("turned" ?r ?\N{U+0279})
            ("turned" ?s ?s)
            ("turned" ?t ?\N{U+0287})
            ("turned" ?u ?n) ; approx
            ("turned" ?v ?\N{U+028C})
            ("turned" ?w ?\N{U+028D})
            ("turned" ?x ?x)
            ("turned" ?y ?\N{U+028E})
            ("turned" ?z ?z)
        )
    )
    (let
        (
            (rangename (car rangedef))
            (ach (cadr rangedef)) ; start of regular range
            (uch (nth 2 rangedef)) ; start of alt range
            (uhi
                (cond
                    ((> (length rangedef) 3) (nth 3 rangedef))
                    (t (nth 2 rangedef))
                ) ; cond
            ) ; end of alt range
            rangetab
        )
        (setq rangetab (gethash rangename alt-alphanumerics))
        (unless rangetab
            (setq rangetab (make-hash-table))
            (puthash rangename rangetab alt-alphanumerics)
        ) ; unless
        (while (<= uch uhi)
            (puthash ach uch rangetab)
            (puthash uch ach unalt-alphanumerics)
            (setq uch (+ 1 uch))
            (setq ach (+ 1 ach))
        ) ; while
    ) ; let
) ; dolist

(defun to-alt-alphanumerics (beg end rangename)
    "converts alphanumerics in the selection to alt codes in the specified range."
    ; (interactive "*rsRangename:") ; can’t get this to work
    ;(unless (use-region-p)
    ;    (ding)
    ;    (keyboard-quit)
    ;) ; unless
    (let
        (
            (rangetab (gethash rangename alt-alphanumerics))
            intext
            ac
            uc
        )
        (unless rangetab
            (message (format "Unknown alt-alpha range “%s”" rangename))
            (ding)
            (keyboard-quit)
        ) ; unless
        (setq intext (delete-and-extract-region beg end))
        (deactivate-mark)
        (dotimes (i (- end beg))
            (setq ac (elt intext i))
            (setq uc (gethash ac rangetab))
            (when uc
                (setq ac uc)
            ) ; when
            (insert-char ac)
        ) ; dotimes
    ) ; let
) ; to-alt-alphanumerics

(dolist
    ; define the commands for converting regular text to each alternate alphanumeric range
    (convdef
        '( ; funcname rangename
            ; "double-struck italic" -- only partial
            (to-circled-alt-alpha "circled")
            (to-parenthesized-alt-alpha "parenthesized")
            (to-full-stop-alt-alpha "full stop")
            (to-comma-alt-alpha "comma")
            (to-double-circled-alt-alpha "double circled")
            (to-negative-circled-alt-alpha "negative circled")
            (to-dingbat-circled-sans-serif-alt-alpha "dingbat circled sans-serif")
            (to-dingbat-negative-circled-sans-serif-alt-alpha "dingbat negative circled sans-serif")
            (to-fullwidth-alt-alpha "fullwidth")
            (to-squared-alt-alpha "squared")
            (to-negative-squared-alt-alpha "negative squared")
            (to-double-struck-alt-alpha "double-struck")
            (to-mathematical-bold-alt-alpha "mathematical bold")
            (to-mathematical-italic-alt-alpha "mathematical italic")
            (to-mathematical-bold-italic-alt-alpha "mathematical bold italic")
            (to-mathematical-script-alt-alpha "mathematical script")
            (to-mathematical-bold-script-alt-alpha "mathematical bold script")
            (to-mathematical-bold-fraktur-alt-alpha "mathematical bold fraktur")
            (to-mathematical-sans-serif-alt-alpha "mathematical sans-serif")
            (to-mathematical-sans-serif-bold-alt-alpha "mathematical sans-serif bold")
            (to-mathematical-sans-serif-italic-alt-alpha "mathematical sans-serif italic")
            (to-mathematical-sans-serif-bold-italic-alt-alpha "mathematical sans-serif bold italic")
            (to-mathematical-monospace-alt-alpha "mathematical monospace")
            (to-turned "turned")
        )
    )
    (let
        (
            (funcname (car convdef))
            (rangename (cadr convdef))
        )
        ;(unless (boundp funcname)
        ;    (setq funcname (make-symbol funcname))
        ;) ; unless
        (setf (symbol-function funcname)
            (lambda (beg end)
                (interactive "*r")
                (to-alt-alphanumerics beg end rangename)
            ) ; lambda
        )
    ) ; let
) ; dolist

(setq lexical-binding nil) ; so deactivate-mark can be locally bound

(defun from-alt-alphanumerics (beg end)
    "converts alternative forms of alphanumerics in the selection to their regular forms."
    (interactive "*r")
    (unless (use-region-p)
        (ding)
        (keyboard-quit)
    ) ; unless
    (let
        (
            deactivate-mark ; doesn’t seem to work -- not even with lexical-binding disabled
            (intext (delete-and-extract-region beg end))
            ac
            uc
        )
        (dotimes (i (- end beg))
            (setq uc (elt intext i))
            (setq ac (gethash uc unalt-alphanumerics))
            (when ac
                (setq uc ac)
            ) ; when
            (insert-char uc)
        ) ; dotimes
    ) ; let
) ; from-alt-alphanumerics

(defun flip-text (beg end)
    "flips the order of text in the selection. Useful with turned text."
    ; Would you believe, Emacs doesn’t already provide a function to do this.
    (interactive "*r")
    (unless (use-region-p)
        (ding)
        (keyboard-quit)
    ) ; unless
    (let
        (
            (intext (delete-and-extract-region beg end))
            (count (- end beg))
        )
        (dotimes (i count)
            (insert-char (elt intext (- count i 1)))
        ) ; dotimes
    ) ; let
) ; flip-text
