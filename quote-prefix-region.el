;+
; Insert or remove a quoting prefix into/from lines in the selected
; region.
;
; Note “loop” macro is defined in main dotemacs file.
;
; Copyright 2024 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
; Licensed under CC-BY <http://creativecommons.org/licenses/by/4.0/>.
;-

(defvar quote-prefix-char ">" "quote-prefix character to use")

(defun quote-prefix-region (beg end)
    "adds one level of quote-prefixing to the lines in the selected region."
    (interactive "*r")
    (unless (use-region-p)
        (ding)
        (keyboard-quit)
    ) ; unless
    (let ((end_marker (copy-marker end)))
        (save-excursion
            (goto-char beg)
            (while (< (point) (marker-position end_marker))
                (cond
                    ((looking-at-p (concat (regexp-quote quote-prefix-char) "+ "))
                        (insert quote-prefix-char)
                    )
                    ((looking-at-p (concat (regexp-quote quote-prefix-char) "+$"))
                        (insert quote-prefix-char)
                    )
                    ((looking-at-p "$")
                        (insert quote-prefix-char)
                    )
                    (t
                        (insert (concat quote-prefix-char " "))
                    )
                ) ; cond
                (forward-line)
            ) ; while
        ) ; save-excursion
    ) ; let
) ; quote-prefix-region

(defun quote-unprefix-region (beg end)
    "strips one level of quote-prefixing from all lines in the region."
    (interactive "*r")
    (unless (use-region-p)
        (ding)
        (keyboard-quit)
    ) ; unless
    (let ((end_marker (copy-marker end)))
        (save-excursion
            (goto-char beg)
            (while (< (point) (marker-position end_marker))
                (cond
                    (
                        (looking-at-p
                            (concat
                                (regexp-quote (concat quote-prefix-char quote-prefix-char))
                                "+ "
                            )
                        )
                        (delete-char 1)
                    )
                    (
                        (looking-at-p
                            (concat
                                (regexp-quote (concat quote-prefix-char quote-prefix-char))
                                "+$"
                            )
                        )
                        (delete-char 1)
                    )
                    ((looking-at-p (concat (regexp-quote quote-prefix-char) " "))
                        (delete-char 2)
                    )
                    ((looking-at-p (concat (regexp-quote quote-prefix-char) "$"))
                        (delete-char 1)
                    )
                ) ; cond
                (forward-line)
            ) ; while
        ) ; save-excursion
    ) ; let
) ; quote-unprefix-region
