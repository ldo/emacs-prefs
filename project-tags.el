;;; -*- lexical-binding: t; -*-
;+
; project-tags system, which uses an SQLite database to quickly lookup
; definitions of symbols.
;
; Use the separate project-tags command-line tool to manage the tags cache.
;
; Note “mstr” macro is defined in main dotemacs file.
;
; Copyright 2023 Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
; Licensed under CC-BY <http://creativecommons.org/licenses/by/4.0/>.
;-

(defun find-def (beg end)
    (mstr
        "tries to look for the selected symbol in the project-tags cache for the"
        " current project. If found, opens the file and goes to the appropriate line."
        " If more than one candidate file is found, offers a choice of which one to go to."
    )
    (interactive "r")
    (let*
        (
            (project-base-dir
                (string-trim
                    (shell-command-to-string
                        (format "project-tags basedir %s" (shell-quote-argument (buffer-file-name)))
                    )
                )
            )
            (ident (buffer-substring beg end))
            (occur
                (json-parse-string
                    (shell-command-to-string
                        (format "project-tags lookup --json %s %s"
                            (shell-quote-argument project-base-dir) (shell-quote-argument ident)
                        )
                    )
                )
            )
            (defwhere (gethash "defs" occur))
        )
        (cond
            ((= (length defwhere) 1)
                (setq defwhere (elt defwhere 0))
                (find-file (gethash "file" defwhere))
                (goto-line (gethash "line" (elt (gethash "where" defwhere) 0)))
            )
            ((> (length defwhere) 1)
                (let
                    (
                        (pick-buffer (get-buffer-create "*project-tags-picker*"))
                        (make-pick-entry
                            (lambda (file line)
                                (lambda ()
                                    (interactive)
                                    (find-file file)
                                    (goto-line line)
                                ) ; lambda
                            ) ; lambda
                        )
                        (file)
                        (options-start)
                    )
                    (switch-to-buffer pick-buffer)
                    (defvar-local buffer-read-only nil)
                    (setq buffer-read-only nil)
                    (erase-buffer)
                    (insert
                        (format "More than one candidate found for “%s” in “%s”:\n\n"
                            ident project-base-dir
                        )
                    )
                    (setq options-start (point))
                    (dotimes (i (length defwhere))
                        (let*
                            (
                                (pickactions (make-sparse-keymap))
                                (thisdef (elt defwhere i))
                                (file (gethash "file" thisdef))
                                (line (gethash "line" (elt (gethash "where" thisdef) 0)))
                                (beg)
                                (end)
                                (olay)
                            )
                            (dolist (evt '([return] [kp-enter] [mouse-1]))
                                (define-key pickactions evt (funcall make-pick-entry file line))
                            ) ; dolist
                            (setq beg (point))
                            (insert " • ")
                            (insert file)
                            (insert " •")
                            (setq end (point))
                            (insert "\n")
                            (setq olay (make-overlay beg end))
                            (overlay-put olay 'category 'project-tags-picker-elt)
                            (overlay-put olay 'face
                                '(
                                    :box (:line-width (4 . 4) :color "#fff")
                                    :foreground "#222"
                                    :background "#eee"
                                )
                            )
                            (overlay-put olay 'keymap pickactions)
                        ) ; let
                    ) ; dotimes
                    (goto-char options-start)
                    (setq buffer-read-only t)
                ) ; let
            )
            (t
                (message (format "%s not found" ident))
            )
        ) ; cond
    ) ; let
) ; find-def

(global-set-key [?\C-c ?=] 'find-def)
